package com.legacy.fight_stick;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Rarity;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

@EventBusSubscriber(modid = FightStickMod.MODID, bus = Bus.MOD)
public class FightStickRegistry
{
	public static final Item FIGHT_STICK = new FightStickItem(new Item.Properties().group(ItemGroup.COMBAT).rarity(Rarity.EPIC).maxStackSize(1));

	@SubscribeEvent
	public static void onRegisterItems(Register<Item> event)
	{
		register(event.getRegistry(), "fight_stick", FIGHT_STICK);
	}

	private static <T extends IForgeRegistryEntry<T>> void register(IForgeRegistry<T> registry, String name, T object)
	{
		object.setRegistryName(FightStickMod.locate(name));
		registry.register(object);
	}
}