package com.legacy.fight_stick;

import java.util.List;
import java.util.UUID;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class FightStickItem extends Item
{
	public LivingEntity attacker;
	public UUID attackerUuid;

	public LivingEntity victim;
	public UUID victimUuid;

	public FightStickItem(Item.Properties props)
	{
		super(props);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn)
	{
		if (playerIn.isSneaking() && playerIn.isCreative() && this.clearAttackers(playerIn, true).isSuccess())
		{
			return ActionResult.resultSuccess(playerIn.getHeldItem(handIn));
		}

		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

	@Override
	public ActionResultType itemInteractionForEntity(ItemStack stack, PlayerEntity playerIn, LivingEntity target, Hand hand)
	{
		if (playerIn.world.isRemote)
			return ActionResultType.SUCCESS;

		if (!playerIn.isCreative())
		{
			playerIn.sendStatusMessage(translate("not_permitted"), true);
			return ActionResultType.SUCCESS;
		}

		if (target != null)
		{

			// clear the attacker when sneaking
			if (playerIn.isSneaking())
			{
				if (target instanceof MobEntity && ((MobEntity) target).getAttackTarget() != null)
				{
					((MobEntity) target).setAttackTarget(null);

					target.playSound(SoundEvents.BLOCK_BARREL_CLOSE, 1.0F, 0.7F);
					playerIn.sendStatusMessage(translate("clear_target"), true);
				}

				return this.clearAttackers(playerIn, true);
			}

			// select an entity
			else if (this.attackerUuid == null && this.attacker == null && !(target instanceof PlayerEntity))
			{
				target.playSound(SoundEvents.BLOCK_LEVER_CLICK, 1.0F, 2.0F);
				this.attacker = (MobEntity) target;
				this.attackerUuid = target.getUniqueID();
				playerIn.sendStatusMessage(translate("set_generic"), true);

				return ActionResultType.SUCCESS;
			}

			// if they right click on the entity again, clear the attacker
			else if (target.getUniqueID() == this.attackerUuid)
				return this.clearAttackers(playerIn, true);

			// kill each-other!!!!
			else if (this.attackerUuid != null && this.attacker != null)
			{
				this.victim = (LivingEntity) target;

				// whoops can't attack
				if (!(this.attacker instanceof MobEntity && this.victim instanceof MobEntity))
				{
					target.playSound(SoundEvents.BLOCK_ANVIL_LAND, 1.0F, 0.7F);
					playerIn.sendStatusMessage(translate("failure_no_attackers"), true);

					this.clearAttackers(playerIn, false);
					return ActionResultType.PASS;
				}

				target.playSound(SoundEvents.BLOCK_ANVIL_LAND, 1.0F, 2.0F);

				if (this.attacker instanceof MobEntity)
				{
					((MobEntity) this.attacker).spawnExplosionParticle();
					((MobEntity) this.attacker).setAttackTarget(target);
				}

				if (this.victim instanceof MobEntity)
				{
					((MobEntity) this.victim).spawnExplosionParticle();
					((MobEntity) this.victim).setAttackTarget(this.attacker);
				}

				this.attackerUuid = null;
				this.attacker = null;
				playerIn.sendStatusMessage(translate("success"), true);

				this.clearAttackers(playerIn, false);

				return ActionResultType.SUCCESS;

			}

			// in case something goes wrong, make sure they know it.
			else
			{
				target.playSound(SoundEvents.BLOCK_ANVIL_LAND, 1.0F, 0.7F);
				playerIn.sendStatusMessage(translate("failure"), true);

				return ActionResultType.PASS;
			}
		}
		else
		{
			return ActionResultType.PASS;
		}
	}

	public ActionResultType clearAttackers(PlayerEntity playerIn, boolean visualElements)
	{
		if (attackerUuid == null && this.attacker == null && this.victim == null)
			return ActionResultType.FAIL;

		this.attackerUuid = null;
		this.attacker = null;
		this.victim = null;

		if (visualElements)
		{
			playerIn.sendStatusMessage(translate("clear"), true);
			playerIn.playSound(SoundEvents.BLOCK_BARREL_CLOSE, 1.0F, 0.7F);
		}

		return ActionResultType.SUCCESS;
	}

	public static TranslationTextComponent translate(String string)
	{
		return new TranslationTextComponent("info." + FightStickMod.MODID + "." + string);
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn)
	{
		if (!Screen.hasShiftDown())
			tooltip.add(translate("hold_shift").mergeStyle(TextFormatting.GRAY));
		else
		{
			tooltip.add(translate("info1").mergeStyle(TextFormatting.GRAY));
			tooltip.add(translate("info2").mergeStyle(TextFormatting.GRAY));
			tooltip.add(translate("info3").mergeStyle(TextFormatting.GRAY));
			tooltip.add(translate("info4").mergeStyle(TextFormatting.GRAY));
			tooltip.add(translate("info5").mergeStyle(TextFormatting.RED));
		}
	}

	@Override
	public boolean hasEffect(ItemStack stack)
	{
		return true;
	}
}
