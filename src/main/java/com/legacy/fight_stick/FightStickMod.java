package com.legacy.fight_stick;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.Mod;

@Mod(FightStickMod.MODID)
public class FightStickMod
{
	public static final String NAME = "Fight Stick";
	public static final String MODID = "fight_stick";

	public FightStickMod()
	{
	}

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(FightStickMod.MODID, name);
	}

	public static String find(String name)
	{
		return FightStickMod.MODID + ":" + name;
	}
}
